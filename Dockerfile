# Using PHP 7.3
FROM php:7.3-cli

# PHP extensions
RUN apt-get update -y \
    && apt-get install -y --no-install-recommends \
        # Required for ext-gd
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        # Required for ext-intl
        libicu-dev \
        # Required for ext-soap
        libxml2-dev \
        # Required for ext-xsl
        libxslt-dev \
        # Required for ext-zip
        libzip-dev \
    # Configure GD
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    # Install packages
    && docker-php-ext-install -j$(nproc) \
        bcmath \
        gd \
        intl \
        pdo_mysql \
        soap \
        sockets \
        xsl \
        zip

# Composer
COPY --from=composer:2 /usr/bin/composer /usr/local/bin/composer2
COPY --from=composer:1 /usr/bin/composer /usr/local/bin/composer1
RUN ln -s /usr/local/bin/composer1 /usr/local/bin/composer

# Tools
RUN apt-get update -y \
    && apt-get install -y --no-install-recommends \
        # GIT
        git \
        # Grunt, used to compile less templates
        grunt \
        # Less
        less \
        # Node and NPM
        nodejs \
        npm \
        # SSH client
        ssh-client \
        # Rsync
        rsync \
        # (Un)zip
        unzip \
        zip

# PHP configuration
COPY php.ini /usr/local/etc/php/php.ini

# Composer tools
RUN composer global require isaac/composer-velocita

# Deployer
COPY deployer.phar /usr/local/bin/dep

